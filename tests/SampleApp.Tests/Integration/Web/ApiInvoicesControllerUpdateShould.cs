﻿using SampleApp.Core.Entities;
using SampleApp.Web;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace SampleApp.Tests.Integration.Web
{
    using System.Text;

    using SampleApp.Core.Enums;
    using SampleApp.PresentationLayer.ApiModels;

    public class ApiInvoicesControllerUpdateShould : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        private InvoiceDTO _initionalItem;

        public ApiInvoicesControllerUpdateShould(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }


        private async Task<InvoiceDTO> GetInoiceFromDB()
        {

            var response = await _client.GetAsync("/api/invoices");
            response.EnsureSuccessStatusCode();
            var stringResponse = await response.Content.ReadAsStringAsync();
            var invoices = JsonConvert.DeserializeObject<IEnumerable<InvoiceDTO>>(stringResponse).ToList();

            return invoices.First();
        }

        private async Task<InvoiceDTO> UpdateInvoice(InvoiceDTO invoiceForUpdate)
        {
            var invoiceId = invoiceForUpdate.Id;

            dynamic newInvoice = JsonConvert.SerializeObject(invoiceForUpdate);
            var content = new StringContent(newInvoice.ToString(), Encoding.UTF8, "application/json");

            var updateResult = await _client.PatchAsync("/api/invoices",content);
            updateResult.EnsureSuccessStatusCode();

            var response = await _client.GetAsync("/api/invoices/"+invoiceId);
            response.EnsureSuccessStatusCode();
            var stringResponse = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<InvoiceDTO>(stringResponse);
        }

        [Fact]
        public async Task UpdateInvoiceState()
        {

            var invoice = await this.GetInoiceFromDB();

            Assert.Equal(invoice.State, InvoiceState.Created);
            invoice.State = InvoiceState.PartlyPaid;
            var updatedInvoice = await UpdateInvoice(invoice);

            Assert.Equal(updatedInvoice.State, InvoiceState.PartlyPaid);
            Assert.Equal(invoice.Id, updatedInvoice.Id);
        }

        [Fact]
        public async Task UpdateInvoiceItemsDelete()
        {
            var invoice = await this.GetInoiceFromDB();

            var newItem = new ItemDTO
                              {
                                  Price = 999,
                                  Quantity = 999,
                                  Title = "Test string",
                              };

            invoice.Items = new List<ItemDTO> { newItem };

            var updatedInvoice = await UpdateInvoice(invoice);

            Assert.Equal(invoice.Id, updatedInvoice.Id);
            Assert.Equal(updatedInvoice.Items.Count, 1);
            Assert.Equal(updatedInvoice.Items[0].Price, newItem.Price);
            Assert.Equal(updatedInvoice.Items[0].Quantity, newItem.Quantity);
            Assert.Equal(updatedInvoice.Items[0].Title, newItem.Title);
        }

        [Fact]
        public async Task UpdateInvoiceItemsRename()
        {
            var invoice = await this.GetInoiceFromDB();

            var newTitle = "foo";
            
            invoice.Items[0].Title = newTitle;

            var updatedInvoice = await UpdateInvoice(invoice);

            Assert.Equal(invoice.Id, updatedInvoice.Id);
            Assert.Equal(updatedInvoice.Items[0].Title, newTitle);
        }
    }
}
